;
; BIND data file for local loopback interface
;
$TTL	604800
@	IN	SOA	ns1.naimsergi.example.com admin.naimsergi.example.com. (
			      3		; Serial
			 604800		; Refresh
			  86400		; Retry
			2419200		; Expire
			 604800 )	; Negative Cache TTL

; name servers - NS records
    IN      NS      ns1.naimsergi.example.com.

; name servers - A records
ns1.naimsergi.example.com.          IN      A       10.5.2.12

; 10.5.2.0/24 - A records
host1.naimsergi.example.com.        IN      A      10.5.2.30
host2.naimsergi.example.com.        IN      A      10.5.2.31
host3.naimsergi.example.com.        IN      A      10.5.2.32
